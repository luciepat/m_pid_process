#include <Windows.h>
#include <stdio.h>
#include <psapi.h>
#include <tlhelp32.h>

/*
gets cpu use from process ID
in: PID
out: cpu use size
*/
size_t getCPUinfo(DWORD pid) {
	size_t flag; 
	HANDLE process_handle;
	FILETIME creation;
	FILETIME destruction;
	FILETIME kernalTime;
	FILETIME userTime;
	SYSTEMTIME sysTime;

	TCHAR Buffer[MAX_PATH];
	ULARGE_INTEGER user;
	ULARGE_INTEGER kernal;
	ULARGE_INTEGER start;
	ULARGE_INTEGER end;

	double percent; 
	DWORD err;

	process_handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid); // get handle
	if (!process_handle) {
		err =GetLastError();
		return NULL;
	}

	flag = GetProcessTimes(process_handle, &creation, &destruction, &kernalTime, &userTime);
	if (flag) {
		//Until the blank line- conversion from Filetime to ULONG_INTEGERS- to use arithmitic
		memcpy(&user, &userTime, sizeof(user));
		memcpy(&kernal, &kernalTime, sizeof(kernal));
		memcpy(&start, &creation, sizeof(start));
		if (!destruction.dwHighDateTime) {// if process still running need current time
			GetSystemTime(&sysTime);
			SystemTimeToFileTime(&sysTime, &destruction);
		}
		memcpy(&end, &destruction, sizeof(end));

		user.QuadPart = user.QuadPart + kernal.QuadPart; // both user and kernal
		end.QuadPart = end.QuadPart - start.QuadPart;// length of time process is/was running
		percent = (long double)user.QuadPart / (long double)end.QuadPart;
		printf("%f", percent*100);
		CloseHandle(process_handle);
		return percent * 100;
	}
	else {
		err = GetLastError();
	}
	CloseHandle(process_handle);
}

int main(){
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);// get screenshot of open processes

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE) //loop through
		{
			if (_stricmp(entry.szExeFile, "firefox.exe") == 0) // when find desired process 
			{
				
				getCPUinfo(entry.th32ProcessID);
			}
		}
	}

	CloseHandle(snapshot);

	return 0;
}


